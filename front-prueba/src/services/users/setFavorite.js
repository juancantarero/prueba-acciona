import axios from 'axios';
import { URLS } from '../urls';

export async function setFavorite(item) {
    const url = `${URLS.API}/user`;
    try {
        const { data } = await axios.post(url, item);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

import axios from 'axios';
import { URLS } from '../urls';

export async function getListFavorites() {
    const url = `${URLS.API}/user/favorites/list`;
    try {
        const { data } = await axios.get(url);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

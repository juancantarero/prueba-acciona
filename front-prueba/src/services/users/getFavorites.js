import axios from 'axios';
import { URLS } from '../urls';

export async function getFavorites() {
    const url = `${URLS.API}/user/favorites`;
    try {
        const { data } = await axios.get(url);
        return data;
    } catch (error) {
        throw new Error(error);
    }
};

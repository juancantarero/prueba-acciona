import axios from 'axios';
import { URLS } from '../urls';

export async function getUsers(params = {}) {
    const url = `${URLS.RANDOM_USERS}/?page=1&results=100`;
    const queryParams = getQueyParams(params);
    try {
        const { data: { results, info } } = await axios.get(url + queryParams);
        results.forEach(element => {
            element.seed = info.seed;
            element.version = info.version;
            element.nameCompose = `${element.name.first} ${element.name.last}`;
            element.id = element.id.value;
            element.fechaNacimiento = element.dob.date;
            element.fechaRegistro = element.registered.date;
            element.favorite = false;
        });
        return results;
    } catch (error) {
        throw new Error(error);
    }
}

function getQueyParams(params) {
    if (Object.keys(params).length === 0) {
        return '';
    }
    let query = '';
    Object.keys(params).forEach(key => {
        if (key !== 'edad' && params[key]) {
            query += `&${key}=${params[key].toLocaleLowerCase()}`;
        } else if (key === 'edad' && params[key]) {
            query += `&dob.age=${params[key].toLocaleLowerCase()}`;
        }
    });
    return query;
}

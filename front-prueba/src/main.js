import Vue from 'vue';
import App from './App';
import router from './router';
import VueMaterial from 'vue-material';
import axios from 'axios';
import VueAxios from 'vue-axios';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';
import './css/styles.css';
import { LMap, LTileLayer, LMarker, LCircleMarker } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-circle-marker', LCircleMarker);

Vue.use(VueAxios, axios);
Vue.config.productionTip = false;
Vue.use(VueMaterial);

new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
});

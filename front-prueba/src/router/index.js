import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'users',
            component: () => import('../views/Users.vue')
        },
        {
            path: '/user/detail',
            name: 'user-detail',
            component: () => import('../views/UserDetail.vue')
        },
        {
            path: '/user/favorites',
            name: 'user-favorites',
            component: () => import('../views/UserExport.vue')
        }
    ]
});

import moment from 'moment';

export function dateFilter(date = new Date(), format = 'DD/MM/YYYY') {
    return moment(String(date)).format(format);
}

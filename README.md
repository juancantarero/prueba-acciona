# Acciona-Prueba

> Prueba Acciona VueJs - NodeJs
Hecho por Juan Pedro Cantarero Reyes
Linkedin [Juan Pedro Cantarero Reyes](https://www.linkedin.com/in/juan-pedro-cantarero-reyes-432877150/)
Correo juan.cantareroreyes@gmail.com

## Requisitos

### Tener nodeJs instalado.

## Instalación

``` bash
# Instalar dependencias de VueJs y NodeJs
install.bat

```

## Ejecución
``` bash
# iniciar proyectos
start.bat

```


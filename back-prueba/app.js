const express = require('express');
var cors = require('cors');
const app = express();
const port = 3000;
const favList = [];


app.use(express.json());
app.use(cors());

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});

app.get('/', (req, res) => {
    res.send('Hello World!')
});

app.post('/user', (req, res) => {
    const { user, props } = req.body;
    let list;
    let isNewList = false;

    if (!user || !props) {
        return res.status(400).send();
    }

    if (props.id) {
        list = getFavListById(props);
    } else {
        isNewList = true;
        list = {
            props: {
                id: Math.floor((Math.random() * 10000) + 1),
                text: props.text
            },
            users: []
        }
        favList.push(list);
    }

    const index = list.users.findIndex(userToFind => user.login.uuid === userToFind.login.uuid);
    const listToModify = favList.find(listToFind => listToFind.props.id === list.props.id);
    if (index > -1 && !user.favorite) {
        listToModify.users.splice(index, 1);
    } else if (user.favorite) {
        listToModify.users.push(user);
    }

    res.send(JSON.stringify(user));
});

app.get('/user/favorites', (req, res) => {
    res.send(JSON.stringify(favList))
});

app.get('/user/favorites/list', (req, res) => {
    const listProps = favList.map(list => list.props);
    res.send(JSON.stringify(listProps))
});

getFavListById = (props) => {
    return favList.find(list => list.props.id === props.id);
};
